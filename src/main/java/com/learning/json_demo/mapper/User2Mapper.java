package com.learning.json_demo.mapper;

import com.learning.json_demo.domain.User2;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * mybatis
 * dao层，sql语句，实现数据库的增删改查  1.13
 * 在main（）文件需要加下边配置
 * @MapperScan("com.learning.json_demo.mapper")   //扫描mappper文件夹中的数据库操作  mybatis
 * mapper:映射器
 */
@Component
public interface User2Mapper {

    @Insert("INSERT INTO user2(name,phone,create_time,age) VALUES(#{name}, #{phone}, #{createTime}, #{age})")
    //在插入数据user后，自动获取主键值id
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
        //keyProperty java对象的属性；keyColumn表示数据库的字段
    int Insert(User2 user2);


    @Select("SELECT * FROM USER2")
    @Results({                         //指定类中的createTime对应表中的create_time
            @Result(column = "create_time", property = "createTime")
    })
    List<User2> getAllUser2();

    @Select("SELECT * FROM USER2 WHERE id =#{id}")
    @Results({
            @Result(column = "create_time", property = "createTime")
    })
    User2 findById(Integer id);

    @Delete("DELETE FROM USER2 WHERE id = #{id}")
    void removeById(Integer id);

    @Update("UPDATE USER2 SET name =#{name} WHERE id = #{id} ")
        //更新数据时候需要通过整条数据
    void updateUser2(User2 user2);


}

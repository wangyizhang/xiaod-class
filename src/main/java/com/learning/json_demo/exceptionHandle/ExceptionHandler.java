package com.learning.json_demo.exceptionHandle;

import com.learning.json_demo.exception.MyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 异常处理  1.9
 */
@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(value = MyException.class)   //处理的异常类型
    @ResponseBody
    public Object exceptionHandler(Exception e) {
        ModelAndView modelAndView = new ModelAndView();                //ModelAndView:spring mvc 中的页面跳转类
        modelAndView.setViewName("error.html");                       //设置跳转到的页面
        modelAndView.addObject("msg", e.getMessage());   //添加错误信息，本例无用
        return modelAndView;
    }


}

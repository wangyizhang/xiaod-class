package com.learning.json_demo.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * 监听器  1.10
 * 三种常用的监听器：
 * ServletContextListener、ServletRequestListener、HttpSessionListener
 */
@WebListener
public class RequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {         //销毁
        System.out.println("=====destroy listener====");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {     //初始化
        System.out.println("====init listener====");

    }
}

package com.learning.json_demo.controller;

import com.learning.json_demo.domain.JsonData;
import com.learning.json_demo.task.AsyncTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Future;

/**
 * 异步任务 1.16
 * @EnableAsync               //开启异步任务  主方法内
 */

@RestController
public class AsyncController {

    @Autowired
    private AsyncTask asyncTask;

    @GetMapping(value = "async")
    private JsonData tryAsync() throws InterruptedException {    //异步运行
        Long begin = System.currentTimeMillis();
        asyncTask.task1();      //1s
        asyncTask.task2();      //2s
        asyncTask.task3();      // 3s
        Long end = System.currentTimeMillis();
        System.out.println("总花费时间： " + (end - begin));
        return new JsonData(0, "success");
    }

    //获取异步结果
    @GetMapping(value = "get_result")
    private JsonData getAsyncResult() throws InterruptedException {    //异步运行
        Long begin = System.currentTimeMillis();
        Future<String> task4 = asyncTask.task4();    //2s
        Future<String> task5 = asyncTask.task5();    //3s
        Future<String> task6 = asyncTask.task6();    //1s
        for (; ; ) {                                       //获取结果前要确保所有操作都正确执行了
            if (task4.isDone() && task5.isDone() && task6.isDone()) {
                break;
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("总花费时间： " + (end - begin));
        return new JsonData(1, "success");
    }


}

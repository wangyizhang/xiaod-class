package com.learning.json_demo.controller;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * Interceptor拦截器
 */
public class InterceptorController {


    //测试Interceptor拦截器
    @GetMapping(value = "/configurer/test")
    public String configurer() {
        System.out.println("Interceptor.......");
        return "Lucy";
    }

}

package com.learning.json_demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * filter:过滤器
 */
@RestController
public class FilterController {

    @RequestMapping(value = "/api/account", method = RequestMethod.GET)
    public Object login(@RequestParam("username") String username) {
        return username;
    }
}

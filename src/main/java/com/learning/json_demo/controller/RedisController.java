package com.learning.json_demo.controller;

import com.learning.json_demo.domain.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试redis  1.16
 */

@RestController
public class RedisController {

    @Autowired
    private StringRedisTemplate redisTpl; //jdbcTemplate

    @GetMapping(value = "add")
    public Object add() {

        //opsForValue : Returns the operations performed on simple values (or Strings in Redis terminology).

        redisTpl.opsForValue().set("name", "wang yi zhang");      //  redisTpl.opsForList().set();
        return new JsonData(0, "add");


    }

    @GetMapping(value = "get")
    public Object get() {

        String value = redisTpl.opsForValue().get("name");
        return new JsonData(1, "get", value);

    }


}

package com.learning.json_demo.controller;

/**
 * 文件映射，文件上传
 */

import com.learning.json_demo.domain.JsonData;
import com.learning.json_demo.domain.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
public class FileController {

    //页面跳转
    @RequestMapping(value = "v1/getFile")             //需要thymeleaf模板
    public Object getFile() {
        return "index";       //返回index.html页面
    }

    //保存文件的地址
    private static final String filePath = "\\E:\\idea_test2\\json_demo\\src\\main\\resources\\static\\images\\";

    //注意html中method="POST" 所以此处为Post方法
    @RequestMapping(value = "upload")    //指定页面URL,对应upload.html中的form表单中的action="/upload"
    @ResponseBody
    public JsonData uploadFile(@RequestParam("head_img") MultipartFile file, HttpServletRequest request) {

        //file.isEmpty(); 判断图片是否为空
        //file.getSize(); 图片大小进行判断

        String name = request.getParameter("name");
        System.out.println("用户名：" + name);

        // 获取文件名
        String fileName = file.getOriginalFilename();
        System.out.println("上传的文件名为：" + fileName);

        // 获取文件的后缀名,比如图片的jpeg,png
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        System.out.println("上传的后缀名为：" + suffixName);

        // 文件上传后的路径
        fileName = UUID.randomUUID() + suffixName;   //UUID.randomUUID() ：随机产生的名称
        System.out.println("转换后的名称:" + fileName);

        File dest = new File(filePath + fileName);

        try {
            file.transferTo(dest);      //文件传到哪里

            return new JsonData(0, fileName);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JsonData(-1, "fail to save ", null);
    }


    //保存文件的地址
    private final static String filePath2 = "\\E:\\idea_test2\\json_demo\\src\\main\\resources\\static\\images\\";

    @RequestMapping(value = "/uploadFile")
    @ResponseBody
    public Result uploadFile2(@RequestParam("myFile") MultipartFile file, HttpServletRequest request) {
        String username = request.getParameter("myName");
        System.out.println("姓名:  " + username);

        String fileName = file.getOriginalFilename();  //获取文件名
        System.out.println("文件名： " + fileName);

        String surfix = fileName.substring(fileName.lastIndexOf("."));
        System.out.println("后缀： " + surfix);

        try {
            File destoryFile = new File(filePath2 + fileName);  //新建文件
            file.transferTo(destoryFile);       //保存文件
            return new Result(0, "success", fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(1, "failure", null);

    }


}

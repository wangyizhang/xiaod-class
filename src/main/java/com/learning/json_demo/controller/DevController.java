package com.learning.json_demo.controller;

import com.learning.json_demo.domain.User;
import com.learning.json_demo.exception.MyException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 热加载
 */

@RestController
public class DevController {

    @GetMapping(value = "/getUser")
    public Object getUser(/*@RequestParam("username") String username,
                          @RequestParam("age") Integer age,
                          @RequestParam("birthday") Date*/) {
        User user = new User("李白", 25, new Date());
        System.out.println("热加载22221111");
        System.out.println("热加载3333");
        return user;
    }



}

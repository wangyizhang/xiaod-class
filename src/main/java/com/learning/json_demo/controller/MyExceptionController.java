package com.learning.json_demo.controller;

import com.learning.json_demo.exception.MyException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 自定义异常处理
 */
@RestController
public class MyExceptionController {

    //产生自定义异常
    @GetMapping(value = "/error")
    public void getException() {
        throw new MyException(100, "this is myException");
    }

}

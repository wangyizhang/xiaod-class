package com.learning.json_demo.controller;

import org.springframework.web.bind.annotation.GetMapping;

/**
 * Listener 测试监听
 * 需要在启动类中加注解@ServletComponentScan  来扫描监听器配置文件
 */
public class ListenerController {


    //测试监听listener
    @GetMapping(value = "/test/listener")
    public String listener() {
        System.out.println("listenering.......");
        return "wang yi zhang";
    }
}

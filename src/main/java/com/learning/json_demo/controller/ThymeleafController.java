package com.learning.json_demo.controller;

import com.learning.json_demo.domain.ServletSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * thymeleaf测试模板
 * 2019.1.12
 */

@Controller
public class ThymeleafController {

    @Autowired
    private ServletSetting servletSetting;


    @GetMapping(value = "thymeleaf")
    public String getIndex(ModelMap modelMap) {
        // modelMap.addAttribute("model",servletSetting);

        return "tl/index";
    }

    @GetMapping(value = "getaccount")
    public String getAccount(ModelMap modelMap) {
        modelMap.addAttribute("model", servletSetting);
        return "tl/account";
    }


}

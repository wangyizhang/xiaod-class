package com.learning.json_demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 通过jedis操作redis  1.15
 */
@RestController
public class JedisController {

    @GetMapping(value = "jedis")
    public void printJedis() {
        //1.设置IP地址和端口
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //2.保存数据
        jedis.set("name2", "杜甫");
        //3.获取数据
        String name = jedis.get("name2");
        System.out.println("name2  " + name);
        //4.释放资源
        jedis.close();

    }

    /**
     * 通过连接池方式连接redis
     */
    @GetMapping(value = "bypool")
    public void getStringByPool() {
        //获取连接池的配置对象
        JedisPoolConfig config = new JedisPoolConfig();
        //设置最大连接数
        config.setMaxTotal(30);
        //设置最大空闲连接数
        config.setMaxIdle(10);
        //获取连接池
        JedisPool jedisPool = new JedisPool(config, "127.0.0.1", 6379);
        //获取核心对象
        Jedis jedis = null;
        try {
            //通过连接池获取连接
            jedis = jedisPool.getResource();
            //设置数据
            jedis.set("name3", "Jack");
            //读取数据
            String name = jedis.get("name3");
            System.out.println("name3  " + name);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放资源
            if (jedis != null) {
                jedis.close();
            }
            if (jedisPool != null) {
                jedisPool.close();
            }
        }

    }


}

package com.learning.json_demo.controller;

import com.learning.json_demo.domain.JsonData;
import com.learning.json_demo.domain.User2;
import com.learning.json_demo.mapper.User2Mapper;
import com.learning.json_demo.service.impl.User2ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 测试MyBatis连接数据库 增删改查 1.13
 * 使用MyBatis时候，数据库中表格需要自己创建
 */
@RestController
public class MybatisController {

    @Autowired
    private User2ServiceImpl user2ServiceImpl;

    @Autowired
    private User2Mapper user2Mapper;

    //插入一条数据
    @GetMapping(value = "addUser2")
    public JsonData addUser2() {
        User2 user2 = new User2();
        user2.setAge(16);
        user2.setCreateTime(new Date());
        user2.setPhone("18216781456");
        user2.setName("Lucy");
        Integer id = user2ServiceImpl.add(user2);
        JsonData jsonData = new JsonData(0, "success", id);
        return jsonData;
    }

    //查询所有数据
    @GetMapping(value = "findAll")
    public JsonData findAllUser2() {
        List<User2> lists = user2Mapper.getAllUser2();
        return new JsonData(0, "success", lists);

    }

    //根据ID查询一条数据
    @RequestMapping(value = "findById", method = RequestMethod.GET)
    public JsonData findById(@RequestParam("id") Integer id) {
        User2 user2 = user2Mapper.findById(id);
        return new JsonData(0, "success", user2);
    }

    //删除一条数据
    @DeleteMapping(value = "deleteById")
    public void removeById(@RequestParam("id") Integer id) {
        user2Mapper.removeById(id);
    }

    //修改user2
    @PostMapping(value = "update")
    public JsonData updateUser2(@RequestParam("name") String name,
                                @RequestParam("id") Integer id) {
        User2 user2 = user2Mapper.findById(id);
        user2.setName(name);
        user2Mapper.updateUser2(user2);
        return new JsonData(0, "success", user2);

    }


}

package com.learning.json_demo.controller;

import com.learning.json_demo.domain.ServerSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class FreemakerController {


    @Autowired
    private ServerSettings setting;


    @GetMapping("freemarker")
    public String index(ModelMap modelMap) {

        modelMap.addAttribute("setting", setting);

        return "index";  //不用加后缀，在配置文件里面已经指定了后缀
    }


}

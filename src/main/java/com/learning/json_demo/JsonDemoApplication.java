package com.learning.json_demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@ServletComponentScan      //自定义过滤器时指定扫描加载
@MapperScan("com.learning.json_demo.mapper")   //扫描mappper文件夹中的数据库操作  mybatis
@EnableScheduling          //开启定时任务，自动扫描
//@ComponentScan("....")   //组件扫描，能把组件中的类以注入的方式使用
@EnableAsync               //开启异步任务
public class JsonDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonDemoApplication.class, args);
    }

    //设置上传文件的大小，需要配置在application文件中
 /*   @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个文件最大
        factory.setMaxFileSize("10240KB"); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize("1024000KB");
        return factory.createMultipartConfig();
    }*/


}


package com.learning.json_demo.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义Servlet  1.10
 * Servlet（Server Applet）是Java Servlet的简称，称为小服务程序或服务连接器，
 * 用Java编写的服务器端程序，主要功能在于交互式地浏览和修改数据，生成动态Web内容。
 */

@WebServlet(name = "userServlet", urlPatterns = "/v1/servlet")     //urlPatterns:指定servlet路径
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().print("userServlet......");
        resp.getWriter().flush();
        resp.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}

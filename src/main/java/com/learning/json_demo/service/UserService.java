package com.learning.json_demo.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserService {

    @GetMapping("/hello")
    public String say() {
        return "hello";
    }
}

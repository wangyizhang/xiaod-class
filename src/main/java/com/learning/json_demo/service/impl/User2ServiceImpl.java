package com.learning.json_demo.service.impl;

import com.learning.json_demo.domain.User2;
import com.learning.json_demo.mapper.User2Mapper;
import com.learning.json_demo.service.User2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

//@Component
@Service
public class User2ServiceImpl implements User2Service {

    @Autowired
    private User2Mapper user2Mapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)    //事物回滚，会占用一部分内存,required就是操作一定需要事物，
    // 没有事物就新建事物，supports是不管有没有事物，操作都能够执行
    public Integer add(User2 user2) {
        user2Mapper.Insert(user2);
        // int a =1/0;             //产生错误，回滚上一步操作
        return user2.getId();
    }
}

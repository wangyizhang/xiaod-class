package com.learning.json_demo.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户登陆过滤器  1.10
 * filter适用于前后端不分离的场景
 * 执行顺序：  1过滤器--2监听器--3拦截器
 * Filter和Interceptor的执行顺序
 * 过滤前->拦截前->action执行->拦截后->过滤后
 */
@WebFilter(urlPatterns = "/api/*", filterName = "loginFilter")   //urlPatterns指明过滤器的作用域
public class LoginFilter implements Filter {

    /**
     * 初始化
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("begin loginFilter");   //初始化方法作用于所有作用域

    }

    /**
     * 执行过滤
     *
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("do loginFilter");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String username = request.getParameter("username");
        if ("wang yi zhang".equals(username)) {
            filterChain.doFilter(request, response);  //放行
        } else {
            response.sendRedirect("/error.html");  //页面跳转
            return;
        }

    }


    /**
     * 销毁
     */
    @Override
    public void destroy() {

        System.out.println("after loginFilter");
    }
}

package com.learning.json_demo.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * scheduled  :   预定的; 排定的; 规定价格的; 严格按时间表生活的
 * 开启定时任务类  1.16
 * 入口程序中加       @EnableScheduling          //开启定时任务，自动扫描
 */

@Component
public class ScheduleTask {

    @Scheduled(fixedRate = 20000)  //每隔20秒执行一次
    public void sum() {
        System.out.println("time :  " + new Date());
    }

}

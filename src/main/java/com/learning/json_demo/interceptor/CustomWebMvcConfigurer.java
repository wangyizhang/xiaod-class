package com.learning.json_demo.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 添加拦截器 1.11
 */
@Configuration  //配置，会被spring boot 扫描到
public class CustomWebMvcConfigurer implements WebMvcConfigurer {

    //添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/configurer/*/**");   //注意格式，添加拦截器(作用范围）

        //	registry.addInterceptor(new TwoIntercepter()).addPathPatterns("/configurer/*/**");  //添加第二个拦截器
        WebMvcConfigurer.super.addInterceptors(registry);
    }

}

package com.learning.json_demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)   //底层junit
@SpringBootTest
public class JsonDemoApplicationTests {

    @Test
    public void contextLoads() {
    }

}

